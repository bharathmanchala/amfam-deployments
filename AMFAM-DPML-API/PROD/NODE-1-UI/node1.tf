resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
           
    type     = "ssh"
    user     = "centos"
    host     = "18.235.54.91"
    # password = var.password   
    private_key = file(AMFAM_DPML_Prod.pem)
  }
     script   = (var.testing.sh)
  } 
}