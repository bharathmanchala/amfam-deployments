resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
           
    type     = "ssh"
    user     = "centos"
    host     = "3.224.121.171"
    # password = var.password   
    private_key = file(AMFAM_DPML_NP_SG_02.pem)
  }
     script   = (var.testing.sh)
  } 
}