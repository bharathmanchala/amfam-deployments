#!/bin/bash
sudo su

cd dpml

cd amfam_dpml

sh StopDPML_UI.sh

mv amfam-dpml-api-1.0.0.jar backup24.jar

git clone  https://bitbucket.org/PragmaEdge_Teams/oracle-amfam-dpml-ui-build/src/master/amfam-dpml-api-1.0.0.jar

mv amfam_dpml/amfam-dpml-api-1.0.0.jar ./

chmod 777 amfam-dpml-api-1.0.0.jar

sh StartDPML_UI.sh