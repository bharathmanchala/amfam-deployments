resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
           
    type     = "ssh"
    user     = "centos"
    host     = "3.217.210.109"
    # password = var.passwor9d   
    private_key = file(AMFAM_DPML_Prod.pem)
  }
     script   = (var.testing.sh)
  } 
}