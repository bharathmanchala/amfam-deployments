resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
           
    type     = "ssh"
    user     = "centos"
    host     = "52.1.146.208"
    # password = var.password   
    private_key = file(AMFAM_DPML_NP.pem)
  }
     script   = (var.testing.sh)
  } 
}