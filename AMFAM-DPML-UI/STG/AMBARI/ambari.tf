resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
           
    type     = "ssh"
    user     = "centos"
    host     = "107.23.226.196"
    # password = var.password   
    private_key = file(AMFAM_DPML_NP_SG_02.pem)
  }
     script   = (var.testing.sh)
  } 
}